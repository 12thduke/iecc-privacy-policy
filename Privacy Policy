PRIVACY POLICY FOR MOBILE APPLICATION IECC CARE

Last Updated: September 26, 2023

This privacy notice for ICP Techno LLC ('we', 'us', or 'our'), describes how and why we might collect, store, 
use, and/or share ('process') your information when you use download and use our mobile application 
IECC Care (the “App”).

Questions or concerns? Reading this privacy notice will help you understand your privacy rights and 
choices. If you do not agree with our policies and practices, please do not use the App. If you still have 
any questions or concerns, please contact us at compliance@icptechno.com.

1. Information We Collect
    a.  Location Data: The App collects your precise location data when you clock in and clock out of your 
        schedule. This information is used for tracking work hours, your work performance and ensuring
        accurate scheduling.
    b.  Clock in and clock out times: the time you clock in and out of a schedule is collected. This information 
        is used for tracking work hours, your work performance and ensuring accurate scheduling. 
    c.  Observations of clients: the client observation data entered in the App will be collected. 

2. How We Use Your Information
    We use the collected location data for the following purposes:
    a.  Clocking In and Out: Location data and time used to record caregivers' work hours accurately and 
        facilitate schedule management.

3. Sharing of Information
    We may share location data with Independent Excel Care Consortium Limited (IECC) for the sole purpose 
    of schedule management and compliance. The information will not be shared with any other party 
    outside the IECC. 

4. Security Measures
    We take reasonable steps to protect personal information, including location data. Access to this 
    information is restricted to authorized personnel who require it for legitimate purposes. However, no 
    online data transmission is entirely secure, and we cannot guarantee absolute security.
    Also, it is essential to understand that you are responsible for the safety and security of your login 
    credentials which will be used to access the application. Any breaches of security that may arise due to 
    the mishandling of personal login credentials fall outside the responsibility of the App. By employing our 
    App, you acknowledge and consent to this risk. 

5. Legal Compliance
    We may share your information to fulfill our legal obligations and cooperate with law enforcement if 
    legally mandated.

6. Changes to this Privacy Policy
    We may update this Privacy Policy as needed to reflect changes in our practices or for legal or regulatory 
    reasons. Any updates will be posted to this page, and the updated Privacy Policy will have an effective 
    date. Your continued use of the App after such changes constitutes your acceptance of the revised 
    Privacy Policy.

7. Contact Us
    If you have any questions or concerns regarding this Privacy Policy, please contact us at 
    compliance@icptechno.com

By using the IECC Care mobile application, you give consent to this Privacy Policy and agree to its terms 
and conditions.

This Privacy Policy is effective as of the date stated at the beginning of this document and supersedes all 
prior versions.

ICP Techno LLC 32 Angampitiya Road Kotte, Western Province 10100 Sri Lanka.